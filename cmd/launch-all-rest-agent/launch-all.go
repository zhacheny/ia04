package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	rad "gitlab.utc.fr/zhacheny/ia04/agt"
	server "gitlab.utc.fr/zhacheny/ia04/agt/ballotagent"
	voter "gitlab.utc.fr/zhacheny/ia04/agt/voteragent"
	"gitlab.utc.fr/zhacheny/ia04/comsoc"
)

func request_new_ballot(id string, votingMethod string, alts int, ddl string, voter_ids []string) {
	req := rad.NewBallot_Request{
		Rule:      votingMethod,
		ID:        id,
		Alts:      alts,
		Deadline:  ddl,
		Voter_ids: voter_ids,
	}

	url := "http://localhost:8000/new_ballot"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	return
}

func request_result(ballot_id string) {
	req := rad.Result_Request{
		Ballot_id: ballot_id,
	}
	url := "http://localhost:8000/result"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	return
}

func RandomPrefs(alts []comsoc.Alternative) (res []comsoc.Alternative) {
	res = make([]comsoc.Alternative, len(alts))
	copy(res, alts)
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(res), func(i, j int) { res[i], res[j] = res[j], res[i] })
	return
}

/*
// @description: un demo de test pour simulation un processus de vote
*/
func main() {
	const n = 100
	const url1 = ":8000"
	const url2 = "http://localhost:8000"
	methds := [...]string{"Majority", "Borda", "Approval", "Condorcet", "Copeland", "STV", "Kemeny"}
	rand.Seed(time.Now().UnixNano())
	methd := methds[rand.Intn(7)]

	clAgts := make([]voter.RestClientAgent, 0, n)
	servAgt := server.NewRestServerAgent(url1)
	log.Println("démarrage du serveur...")
	go servAgt.Start()
	log.Println("démarrage des clients...")
	alts := []comsoc.Alternative{1, 2, 3, 4, 5, 6, 7}
	voter_ids := make([]string, 0)
	for i := 0; i < n; i++ {
		id := fmt.Sprintf("id%02d", i)
		pref := RandomPrefs(alts)
		option := []int{1}
		agt := voter.NewRestClientAgent(id, url2, pref, "vote12", option)
		voter_ids = append(voter_ids, id)
		clAgts = append(clAgts, *agt)
	}
	request_new_ballot("vote12", methd, 7, "Mon Nov 20 19:10:30 UTC 2023", voter_ids)
	for _, agt := range clAgts {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(agt voter.RestClientAgent) {
			go agt.Start()
		}(agt)
	}

	fmt.Scanln()
	request_result("vote12")
}
