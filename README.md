# IA04


## Introduction

Un système multi-agent qui simule le processus de vote, il comprend:
- Un serveur qui handle les requests de créer un ballot agent, voter et obtenir le résultat. Le serveur écoute l'addresse et port  http://localhost:8000
- Les Balllot Agent qui reçoivent les vote , génèreent le Profil et calculer le résultat.
- Les Voteurs qui vote qvec une slice de préférence.


Gitlab repository : [here](https://gitlab.utc.fr/zhacheny/ia04).

---

## Quick Start

### Go Install

Vous pouvez également utiliser la commande **go install** pour commencer.

```go
go install -v gitlab.utc.fr/zhacheny/ia04@latest
```

Si tout se passe bien, vous pouvez trouver le fichier exécutable **ia04** dans dossier **bin** sous le chemin **$GOPATH**. 

````shell
$GOPATH/bin/ia04
````

### Création de ballot de vote : /new_ballot
Avec cette commande, nous pouvons désormais créer un ballot de vote. Pour ce faire, il faut renseigner les informations nécéssaires d'un ballot.
- **ID** de type string. Indique l'identifiant de ce ballot(c'est un peu différent que l'exemple fournie par professeur)
- **rule** de type **string**. Choisir entre **"Condorcet"**, **"Majority"**, **"Borda"**,**"Approval"**, **"Copeland"**, **"coombs"**, **"STV"**, **"Kemeny"**
- **deadline** de type **string**
- **voter-Ids** de type **[string, ...]**
- **alts** de type **int**

![result](./img/new_ballot.png)

Si vous voulez lancer plusieurs votes, changez des informations et envoyez, une nouvelle vote est créée, avec un code **201** retourné.
En cas d'anomalie, **400** est retourné pour **bad request** et **501** pour **not implemented**

#### quelques exemples de l'anomalies
**le deadline qu'on post est incompatible avec notre format ou il est antérieur que maintenant**

![erreur1](./img/ddl_error.png)

**le ballot existe déjà**

![erreur2](./img/ballot_existe_deja.png)

**alts est inférieur à 0**

![erreur3](./img/alts.png)


### Gestion des votants : /vote

Cette commande sert à renseigner les préférences de chaque votants dans tous les ballots.
De même, les informations des champs sont nécéssaires.
- **ballot_id** de type **string**
- **voter_id** de type **string**. Cet ID est celui créé dans **/new_ballot**
- **prefs** de type **[int, ...]**
- **option** de type **int**

![result](./img/vote.png)

En cas d'anomalie, **400** est retourné pour **bad request**, **403** pour **vote déjà effectué**, **501** pour **not implemented** et **503** pour **la deadline est déjà dépassée**

#### quelques exemples de l'anomalies
**dépasser le deadline**

![erreur4](./img/timeout.png)

**voteur a déjà voté**

![erreur5](./img/deja%20vote.png)

**manque une alternative dans préférence**

![erreur5](./img/manque_alt.png)

**doublon d'alternatives**

![erreur5](./img/double_alt.png)

### Résultats : /result
En renseignant **ballot_Id** de type **string** obtenu précedemment(**votex**), nous pouvons obtenir le résultat. Ce dernier comprend deux partie : le gagnant


Voir l'exemple ci-dessous.
![result](./img/resultat.png)

#### Quelques exemples de l'anomalies
**le ballot qu'on recherche n'existe pas**

![erreur5](./img/ballot_notfound.png)

**deadline est dépassé**

![erreur5](./img/timeout.png)

### Fichier exécutable
Le fichier exécutable nous donne la possibilité de savoir l'état de chaque commande. Voici un exemple d'un vote sans erreurs.

![executable](./img/exe.png)

