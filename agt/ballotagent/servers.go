package ballotagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	agt "gitlab.utc.fr/zhacheny/ia04/agt"
	comsoc "gitlab.utc.fr/zhacheny/ia04/comsoc"
)

type RestServerAgent struct {
	sync.Mutex
	addr         string
	ballotAgents map[string]*RestBallotAgent
}

type RestBallotAgent struct {
	id       string
	rule     string
	deadline string
	voterIds map[string]bool
	options  []int
	nbAlts   int
	bestAlts []comsoc.Alternative
	prof     comsoc.Profile
}

// tous les vote algorithme
var rule = map[string]int{
	"Condorcet": 0,
	"Majority":  1,
	"Borda":     2,
	"Approval":  4,
	"Copeland":  5,
	"STV":       6,
	"Kemeny":    7,
}

/*
// @description: creer un serveur
// @param: addr
*/
func NewRestServerAgent(addr string) *RestServerAgent {
	ballot_agents := make(map[string]*RestBallotAgent)
	return &RestServerAgent{
		addr:         addr,
		ballotAgents: ballot_agents,
	}
}

/*
// @description: creer un BallotAgent
// @param: agent_id, votingmethod,alts, deadline, voter_ids
*/
func NewRestBallotAgent(agent_id string, votingMethod string, alts int, deadline string, voter_ids []string) *RestBallotAgent {
	prof := make(comsoc.Profile, 0)
	bestAlts := make([]comsoc.Alternative, 0)
	voterIds := make(map[string]bool)
	options := make([]int, 0)
	for _, voter_id := range voter_ids {
		voterIds[voter_id] = false
	}
	return &RestBallotAgent{
		id:       agent_id,
		rule:     votingMethod,
		deadline: deadline,
		voterIds: voterIds,
		options:  options,
		nbAlts:   alts,
		bestAlts: bestAlts,
		prof:     prof,
	}
}

/*
// @description: Obtenir le gagnant d'apres le rule
*/
func (rba *RestBallotAgent) GetBestAlt() (winner []comsoc.Alternative) {
	var err error
	switch rba.rule {
	case "Majority":
		rba.bestAlts, err = comsoc.MajoritySCF(rba.prof)
	case "Borda":
		rba.bestAlts, err = comsoc.BordaSCF(rba.prof)
	case "Approval":
		rba.bestAlts, err = comsoc.ApprovalSCF(rba.prof, rba.options)
	case "Condorcet":
		rba.bestAlts, err = comsoc.CondorcetWinner(rba.prof)
	case "Copeland":
		rba.bestAlts, err = comsoc.CopelandSCF(rba.prof)
	case "STV":
		rba.bestAlts, err = comsoc.STV_SCF(rba.prof)
	case "Kemeny":
		rba.bestAlts = append(rba.bestAlts, comsoc.KemenySCF(rba.prof))
	default:
		fmt.Println("Unknown voting method.")
	}
	if err != nil {
		// exit with error
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
	return rba.bestAlts
}

/*
// @description: vérifier si le requete est post ou pas
// @param: method string, w http.ResponseWriter, r *http.Request
*/
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprintln(w, "Not Implemented")
		return false
	}
	return true
}

func (*RestServerAgent) decode_newballot_request(r *http.Request) (req agt.NewBallot_Request, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decode_vote_request(r *http.Request) (req agt.Vote_Request, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decode_result_request(r *http.Request) (req agt.Result_Request, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

/*
// @description: traitement de request de créer un nouveau ballot
// @param: http.ResponseWriter,  http.Request
*/
func (rsa *RestServerAgent) do_newballot_request(w http.ResponseWriter, r *http.Request) {
	log.SetFlags(log.Ldate | log.Ltime)
	log.Println("Reçoit une requete de new_ballot")
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decode_newballot_request(r)
	if err != nil {
		// 400 bad requests
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	// Vérifier l'heure d'échéance
	t1, e1 := time.ParseInLocation("Mon Jan 2 15:04:05 UTC 2006", req.Deadline, time.Local)
	t2, _ := time.ParseInLocation("Mon Jan 2 15:04:05 UTC 2006", time.Now().Format("Mon Jan 2 15:04:05 UTC 2006"), time.Local)
	if e1 != nil || t1.Unix()-t2.Unix() <= 0 {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(": Deadline error")
		fmt.Fprintln(w, "Deadline error")
		return
	}

	// vérifier Alts
	if req.Alts <= 1 {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(": Nombre de Alternatives error")
		fmt.Fprintln(w, "Nombre de Alternatives error")
		return
	}

	// vérifier Rule
	_, ok := rule[req.Rule]
	if ok {
		_, fine := rsa.ballotAgents[req.ID]
		if fine {
			w.WriteHeader(403)
			log.Println(": Ballot déjà existe")
			fmt.Fprintln(w, "Ballot déjà existe")
			return
		} else {
			// traitement de la requête
			new_ballot_agent := NewRestBallotAgent(req.ID, req.Rule, req.Alts, req.Deadline, req.Voter_ids)
			rsa.ballotAgents[req.ID] = new_ballot_agent
			// 201 vote cree
			w.WriteHeader(201)
			msg, _ := json.Marshal(agt.NewBallot_Response{Signfication: "vote créé", NewBallot: req.ID})
			w.Write(msg)
			log.Println("Créer un ballot: ", req.ID)
		}
	} else {
		w.WriteHeader(http.StatusNotImplemented)
		fmt.Fprintln(w, "On ne prend en compte pas votre méthode de vote")
		log.Println("On ne prend en compte pas votre méthode de vote")
	}
}

/*
// @description: Traite tous les requete POST qui est pour l'objectif de voter
// @param: http.ResponseWriter,  http.Request
*/
func (rsa *RestServerAgent) do_vote_request(w http.ResponseWriter, r *http.Request) {
	log.SetFlags(log.Ldate | log.Ltime)
	log.Println("Reçoit une requete de vote")
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decode_vote_request(r)
	if err != nil {
		// 400 bad request
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	ballot, ok := rsa.ballotAgents[req.Vote_id]
	if ok {
		//vérifier le ddl
		time_now, _ := time.ParseInLocation("Mon Jan 2 15:04:05 UTC 2006", time.Now().Format("Mon Jan 2 15:04:05 UTC 2006"), time.Local)
		deadline, _ := time.ParseInLocation("Mon Jan 2 15:04:05 UTC 2006", ballot.deadline, time.Local)
		if deadline.Unix()-time_now.Unix() < 0 {
			w.WriteHeader(503)
			log.Println(": deadline est dépassé")
			fmt.Fprintln(w, "Deadline est dépassé")
			return
		}

		// vérifier la préférence
		if len(req.Prefs) != ballot.nbAlts {
			w.WriteHeader(http.StatusBadRequest)
			log.Println("Vote raté à cause de préférence non autorisée")
			fmt.Fprintln(w, "Vote raté à cause de préférence non autorisée")
			return
		}

		// alternative doublon
		tmpMap := make(map[comsoc.Alternative]int)
		for _, value := range req.Prefs {
			tmpMap[value] = 1
		}
		var keys []interface{}
		for k := range tmpMap {
			keys = append(keys, k)
		}
		if len(keys) != len(req.Prefs) {
			w.WriteHeader(http.StatusBadRequest)
			log.Println("Vote raté à cause de préférence non autorisée")
			fmt.Fprintln(w, "Vote raté à cause de préférence non autorisée")
			return
		}

		// traitement de la requête
		// préciser le seuil si rule est approval
		if len(req.Options) == 0 && ballot.rule == "Approval" {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, err.Error())
			fmt.Fprintln(w, "Il faut préciser le seuil pour le méthode Approval")
			return
		}

		// si le voter a déjà voté
		if ballot.voterIds[req.Agent_id] {
			w.WriteHeader(403)
			fmt.Fprintln(w, "vote déjà effectué")
			return
		}
		// prendre en compte le vote
		ballot.prof = append(ballot.prof, req.Prefs)
		ballot.voterIds[req.Agent_id] = true
		ballot.options = append(ballot.options, req.Options[0])
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "vote pris en compte")
		log.Println("vote pris en compte: ", req.Prefs)
	} else {
		// ballot n'existe pas
		w.WriteHeader(http.StatusNotImplemented)
		log.Println("vote raté parce que ", req.Vote_id, " n'exist pas")
		fmt.Fprintln(w, "vote raté parce que "+req.Vote_id+" n'exist pas")
		return
	}
}

/*
// @description: Traite les requete POST qui est pour l'objectif de obtenir le résultat
// @param: http.ResponseWriter,  http.Request
*/
func (rsa *RestServerAgent) do_result_request(w http.ResponseWriter, r *http.Request) {
	log.SetFlags(log.Ldate | log.Ltime)
	log.Println("Reçoit une requete de result")

	rsa.Lock()
	defer rsa.Unlock()

	// décodage de la requête
	req, decode_err := rsa.decode_result_request(r)
	if decode_err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, decode_err.Error())
		return
	}
	ballot, ok := rsa.ballotAgents[req.Ballot_id]
	//ballot existe
	if ok {
		for _, state_voter := range ballot.voterIds {
			if !state_voter {
				w.WriteHeader(425)
				fmt.Fprintln(w, "Too Early")
				return
			}
		}
		winners := ballot.GetBestAlt()
		winner, _ := comsoc.TieBreak(winners)
		w.WriteHeader(http.StatusOK)
		msg, _ := json.Marshal(agt.Result_Response{Winner: int(winner)})
		w.Write(msg)
		fmt.Printf("La meilleure alternative est %+v via la methode de vote %q\n", winner, ballot.rule)
		return
	} else {
		// le ballot qu'on recherche n'existe pas
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, "Not Found")
	}

}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rsa.do_newballot_request)
	mux.HandleFunc("/vote", rsa.do_vote_request)
	mux.HandleFunc("/result", rsa.do_result_request)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
