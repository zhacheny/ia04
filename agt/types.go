package agt

import (
	"gitlab.utc.fr/zhacheny/ia04/comsoc"
)

type Vote_Request struct {
	Agent_id string               `json:"agent_id"`
	Vote_id  string               `json:"vote_id"`
	Prefs    []comsoc.Alternative `json:"prefs"`
	Options  []int                `json:"options"`
}

type Result_Request struct {
	// Operator string `json:"op"`
	Ballot_id string `json:"ballot_id"`
}

type NewBallot_Request struct {
	ID        string   `json:"id"`
	Rule      string   `json:"rule"`
	Deadline  string   `json:"deadline"`
	Voter_ids []string `json:"voter_ids"`
	Alts      int      `json:"alts"`
}

type NewBallot_Response struct {
	Signfication string `json:"signification"`
	NewBallot    string `json:"newballot"`
}

type Result_Response struct {
	Winner int `json:"winner"`
}
