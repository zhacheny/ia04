package voteragent

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/zhacheny/ia04/agt"
	"gitlab.utc.fr/zhacheny/ia04/comsoc"
)

type RestClientAgent struct {
	id       string
	url      string
	votes    []comsoc.Alternative
	ballotId string
	option   []int
}

func NewRestClientAgent(id string, url string, votes []comsoc.Alternative, ballotId string, option []int) *RestClientAgent {
	return &RestClientAgent{id, url, votes, ballotId, option}
}

func (rca *RestClientAgent) Vote() (err error) {
	req := rad.Vote_Request{
		Agent_id: rca.id,
		Prefs:    rca.votes,
		Options:  rca.option,
		Vote_id:  rca.ballotId,
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	// traitement de la réponse
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	log.SetFlags(log.Ldate | log.Ltime)
	if resp.StatusCode == http.StatusOK {
		log.Println("vote successfully pris en compte")
	} else if resp.StatusCode == http.StatusBadRequest {
		log.Println("Bad Request")
		return errors.New("bad Request")
	} else if resp.StatusCode == http.StatusForbidden {
		log.Println("Vous avez déjà voté")
		return errors.New("vous avez déjà voté")
	} else if resp.StatusCode == http.StatusNotImplemented {
		log.Println("No implemented")
		return errors.New("no implemented")
	} else if resp.StatusCode == 503 {
		log.Println("Deadline dépassé")
		return errors.New("deadline dépassé")
	}
	return nil
}

func (rca *RestClientAgent) Start() {
	log.Printf("démarrage de %s", rca.id)
	err := rca.Vote()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	}
	fmt.Printf("Votant[%s] vote pour les alternatives %+v\n", rca.id, rca.votes)
}
