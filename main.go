package main

import (
	"fmt"

	ras "gitlab.utc.fr/zhacheny/ia04/agt/ballotagent"
)

func main() {
	server := ras.NewRestServerAgent(":8000")
	server.Start()
	fmt.Scanln()
}
