package comsoc

func BordaSWF(p Profile) (Count, error) {
	err := checkProfile(p)
	col := len(p[0])
	ct := make(Count)
	var alt Alternative
	for i := 0; i < len(p); i++ {
		for j := 0; j < col; j++ {
			alt = p[i][j]
			ct[alt] += col - 1 - j
		}
	}
	return ct, err
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	ct, err := BordaSWF(p)
	if err == nil {
		return maxCount(ct), nil
	}
	return nil, err
}
