package comsoc

import "errors"

func TieBreak(alts_slice []Alternative) (alt Alternative, err error) {
	if len(alts_slice) == 0 {
		return -1, errors.New("alternative est vide")
	}
	return alts_slice[0], nil
}

func TieBreakFactory(alts_slice []Alternative) (tie_break func([]Alternative) (Alternative, error)) {
	alts := alts_slice
	alts_static := alts
	return func(alts []Alternative) (a Alternative, e error) {
		if len(alts) == 0 {
			return -1, errors.New("alternative slice est Null")
		}
		result := alts[0]
		for _, alt := range alts {
			if rank(alt, alts_static) < rank(result, alts_static) {
				result = alt
			}
		}
		return result, nil
	}
}
