package comsoc

// comparaison de Condorcet entre deux alternatives
func AlternativeCompareCopeland(prefs Profile, alt Alternative, oppo Alternative) int {
	// comparer un alternative avec un autre ligne par ligne
	// s'il gagne obtient 1 vote, sinon il perd 1 vote
	var vote_yes int
	for i := 0; i < len(prefs); i++ {
		res := isPref(alt, oppo, prefs[i])
		if res {
			vote_yes += 1
		} else {
			vote_yes -= 1
		}
	}
	if vote_yes > 0 {
		return 1
	} else if vote_yes < 0 {
		return -1
	} else {
		return 0
	}
}

func CopelandSWF(prefs Profile) (Count, error) {
	err := checkProfile(prefs)
	ct := make(Count)
	var alts_slice []Alternative
	// initialiser le tableau des alternatives
	for i := 0; i < len(prefs[0]); i++ {
		alts_slice = append(alts_slice, prefs[0][i])
	}
	for i := 0; i < len(alts_slice); i++ {
		point := 0
		alt := Alternative(alts_slice[i])
		for j := 0; j < len(alts_slice); j++ {
			oppo := Alternative(alts_slice[j])
			if alt != oppo {
				point += AlternativeCompareCopeland(prefs, alt, oppo)
			}
		}
		ct[alt] = point
	}
	return ct, err
}

func CopelandSCF(prefs Profile) (bestAlts []Alternative, err error) {
	ct, err_swf := CopelandSWF(prefs)
	best_alts := maxCount(ct)
	return best_alts, err_swf
}
