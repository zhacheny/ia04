package comsoc

func bestAlt_line(pref []Alternative, alts_slice []Alternative, ct Count) Count {
	// le premier alternative(ne pas etre éliminé) d'une ligne de pref gagne 1 vote
	for i := 0; i < len(pref); i++ {
		for _, v := range alts_slice {
			if Alternative(v) == pref[i] {
				ct[Alternative(v)] += 1
				break
			}
		}
		break
	}
	return ct
}

func STV_SWF(prefs Profile) (Count, error) {
	// vérifier le profile
	err := checkProfile(prefs)
	ct := make(Count)
	nb_tour := 0
	alts_slice := ranked_alternatives(prefs[0])
	// initialiser Count avec tous les alternatives
	for i := 0; i < len(alts_slice); i++ {
		ct[Alternative(alts_slice[i])] = 0
	}
	for {
		if len(alts_slice) == 1 {
			break
		} else {
			ct_tmp := make(Count)
			for i := 0; i < len(prefs); i++ {
				ct_tmp = bestAlt_line(prefs[i], alts_slice, ct_tmp)
			}
			// choisir le alternative à éliminer et efface le dans alts_slice
			low_alt := minCount(ct_tmp, alts_slice)
			ct[Alternative(low_alt)] = nb_tour
			for i := 0; i < len(alts_slice); i++ {
				if alts_slice[i] == Alternative(low_alt) {
					alts_slice = append(alts_slice[:i], alts_slice[i+1:]...)
				}
			}
		}
		nb_tour += 1
	}
	ct[Alternative(alts_slice[0])] = nb_tour
	return ct, err
}

func STV_SCF(prefs Profile) ([]Alternative, error) {
	ct, err_swf := STV_SWF(prefs)
	best_alts := maxCount(ct)
	return best_alts, err_swf
}
