package comsoc

import (
	"errors"
	"fmt"
	"sort"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
func checkProfile(prefs Profile) error {

	if len(prefs) == 0 {
		return errors.New("Le profile list est NULL!")
	}

	/// 判读所有的长度是否一致
	length := len(prefs[0])

	for i := 0; i < len(prefs); i++ {
		if len(prefs[i]) != length {
			return errors.New("Profils ne sont pas tous complets!")
		}
	}

	/// 对于每个投票者 判断有无重复的成员
	for i := 0; i < len(prefs); i++ {
		set := make(map[Alternative]bool)
		for j := 0; j < length; j++ {
			if set[prefs[i][j]] == false {
				set[prefs[i][j]] = true
			} else {
				return errors.New("Alternative doublon!")
			}
		}
	}

	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	checkProfile(prefs)
	altsCopy := make([]Alternative, len(alts))
	copy(altsCopy, alts)
	sort.Slice(altsCopy, func(m, n int) bool {
		return altsCopy[m] < altsCopy[n]
	})
	lastPref := Alternative(-1)
	for _, pref := range altsCopy {
		if lastPref == pref {
			return errors.New("doublon alts")
		}
		lastPref = pref
	}
	return nil
}

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if v == alt {
			return i
		}
	}
	return -1 //represent on ne trouve pas alternative
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	var indice_alt1 int = rank(alt1, prefs)
	var indice_alt2 int = rank(alt2, prefs)
	if indice_alt1 < indice_alt2 {
		return true
	} else {
		return false
	}
}

// renvoie les meilleures alternatives pour un décomtpe donné
func maxCount(count Count) (bestAlts []Alternative) {
	var max_count int
	var best_alts []Alternative
	for alt, _ := range count {
		if count[alt] > max_count {
			max_count = count[alt]
			best_alts = best_alts[:0]
			best_alts = append(best_alts, alt)
		} else if count[alt] == max_count {
			best_alts = append(best_alts, alt)
		}
	}
	return best_alts
}

func minCount(count Count, alts []Alternative) (lowAlt Alternative) {
	min_count := count[Alternative(alts[0])]
	var low_alts []Alternative
	for _, alt := range alts {
		if count[Alternative(alt)] < min_count {
			min_count = count[Alternative(alt)]
			low_alts = low_alts[:0]
			low_alts = append(low_alts, Alternative(alt))
		} else if count[Alternative(alt)] == min_count {
			low_alts = append(low_alts, Alternative(alt))
		}
	}
	low_alt, _ := TieBreak(low_alts)
	return low_alt
}

// retourne un slice d'alternatives dans un ordre ascendant
func ranked_alternatives(pref []Alternative) []Alternative {
	var ints_slice []int
	var alts_slice []Alternative
	for i := 0; i < len(pref); i++ {
		ints_slice = append(ints_slice, int(pref[i]))
	}
	sort.Ints(ints_slice)
	for i := 0; i < len(ints_slice); i++ {
		alts_slice = append(alts_slice, Alternative(ints_slice[i]))
	}
	return alts_slice
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît
// qu'une seule fois par préférences
// func checkProfile(prefs Profile) error {
// 	tmpMap := make(map[int]int)
// 	var length int = len(prefs[0])
// 	for i := 0; i < len(prefs); i++ {
// 		if length(prefs[i]) != length {
// 			return error.New("alternative incomplet")
// 		}
// 		for j := 0; j < len(prefs[i]); j++{
// 			map[prefs[i]] += 1

// 		}
// 	}
// 	return nil
// }

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît
// exactement une fois par préférences
// func checkProfileAlternative(prefs Profile, alts []Alternative) error {

// }

func SWF(p Profile) (count Count, err error) {
	ct := make(Count)
	var alt Alternative
	for i := 0; i < len(p); i++ {
		if len(p[i]) != 0 {
			alt = p[i][0]
			ct[alt] += 1
		} else {
			return ct, errors.New("Il n'y a pas d'alternative préféré pour le votant")
		}
	}
	return ct, nil
}

func SCF(p Profile) (bestAlts []Alternative, err error) {
	ct, err := SWF(p)
	if err == nil {
		return maxCount(ct), nil
	}
	return nil, err
}

func SWFFactory(swf func(p Profile) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {
	return func(p Profile) ([]Alternative, error) {
		count_no_order, err_swf := swf(p)
		fmt.Println(count_no_order)
		if err_swf != nil {
			return nil, err_swf
		}
		var count_order []Alternative
		for {
			if len(count_no_order) == 0 {
				break
			} else {
				best_alts := maxCount(count_no_order)
				fmt.Println(best_alts)
				for {
					if len(best_alts) == 0 {
						break
					} else {
						alt, err_tiebreak := tiebreak(best_alts)
						if err_tiebreak != nil {
							return nil, err_tiebreak
						}
						count_order = append(count_order, alt)
						delete(count_no_order, alt)
						best_alts = best_alts[1:]
					}
				}
			}
		}
		return count_order, nil
	}
}

func SCFFactory(scf func(p Profile) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		count, err_scf := scf(p)
		best_alts := maxCount(count)
		best_alt, err_tiebreak := tiebreak(best_alts)
		if err_scf != nil {
			return -1, err_scf
		}
		if err_tiebreak != nil {
			return -1, err_tiebreak
		}
		return best_alt, nil
	}
}
