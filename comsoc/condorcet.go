package comsoc

// comparaison de Condorcet entre deux alternatives
func AlternativeCompare(prefs Profile, alt Alternative, oppo Alternative) bool {
	var vote_yes int
	var vote_no int
	for i := 0; i < len(prefs); i++ {
		res := isPref(alt, oppo, prefs[i])
		if res {
			vote_yes += 1
		} else {
			vote_no += 1
		}
	}
	if vote_yes >= vote_no {
		return true
	} else {
		return false
	}
}

func CondorcetWinner(prefs Profile) ([]Alternative, error) {
	err := checkProfile(prefs)
	var alts_slice []Alternative
	var winner []Alternative
	var flag bool = true
	for i := 0; i < len(prefs[0]); i++ {
		alts_slice = append(alts_slice, prefs[0][i])
	}
	for i := 0; i < len(alts_slice); i++ {
		alt := Alternative(alts_slice[i])
		for j := 0; j < len(alts_slice); j++ {
			oppo := Alternative(alts_slice[j])
			if alt != oppo {
				flag = AlternativeCompare(prefs, alt, oppo)
				if !flag {
					break
				}
			}
		}
		if flag {
			winner = append(winner, alt)
		}
	}
	return winner, err
}
