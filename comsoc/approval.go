package comsoc

func ApprovalSWF(p Profile, thresholds []int) (Count, error) {
	err := checkProfile(p)
	ct := make(Count)
	var alt Alternative
	for i := 0; i < len(p); i++ {
		for j := 0; j < thresholds[i]; j++ {
			alt = p[i][j]
			ct[alt] += 1
		}
	}
	return ct, err
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	ct, err := ApprovalSWF(p, thresholds)
	if err == nil {
		return maxCount(ct), nil
	}
	return nil, err
}
