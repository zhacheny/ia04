package comsoc

type Pair struct {
	e1 int
	e2 int
}

func (p *Pair) Equal(p2 Pair) bool {
	if p.e1 == p2.e1 && p.e2 == p2.e2 {
		return true
	}
	return false
}

func calculDistanceEdition(pref1 []Alternative, pref2 []Alternative) float64 {
	pairs1 := make([]Pair, 0)
	pairs2 := make([]Pair, 0)
	for i := 0; i < len(pref1); i++ {
		for j := i + 1; j <= len(pref1)-1; j++ {
			pairs1 = append(pairs1, Pair{int(pref1[i]), int(pref1[j])})
		}
	}
	for i := 0; i < len(pref1); i++ {
		for j := i + 1; j <= len(pref1)-1; j++ {
			pairs2 = append(pairs2, Pair{int(pref2[i]), int(pref2[j])})
		}
	}
	count := 0
	for _, pair1 := range pairs1 {
		for _, pair2 := range pairs2 {
			if pair1.Equal(pair2) {
				count++
				break
			}
		}
	}
	nbdeconcord := len(pairs1) - count
	disEdition := float64((count - nbdeconcord))
	// fmt.Println("distance d'edition = ", disEdition)
	tau := disEdition / float64(len(pairs1))
	// fmt.Println("tau = ", tau)
	return tau
}

// Kemeny = recherche d'un rangement minimisant la distance entre lui-meme et le profil
// Algorithme = pour chaque rangement de preferences, calculer la distance entre lui et le profil, choisir le profil dont la distance est min
func calculDistanceRangeProfile(pref []Alternative, profil Profile) float64 {
	sum := 0.0
	for _, prefprofil := range profil {
		sum += calculDistanceEdition(pref, prefprofil)
	}
	return sum
}

func KemenySWF(profil Profile) []Alternative {
	minDistance := calculDistanceRangeProfile(profil[0], profil)
	minIndex := 0
	distance := 0.0
	for i, prefs := range profil {
		// fmt.Println("index", i)
		// fmt.Println("Distance de ", prefs, " est ", calculDistanceRangeProfile(prefs, profil))
		if distance = calculDistanceRangeProfile(prefs, profil); distance < minDistance {
			minDistance = distance
			minIndex = i
		}
	}
	return profil[minIndex]
}

func KemenySCF(profil Profile) Alternative {
	return KemenySWF(profil)[0]
}
