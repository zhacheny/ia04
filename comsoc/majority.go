package comsoc

import "fmt"

func MajoritySWF(p Profile) (count Count, err error) {
	err = checkProfile(p)
	ct := make(Count)
	for _, pref := range p {
		ct[pref[0]]++
	}
	fmt.Println(ct)
	return ct, err
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	ct, err := MajoritySWF(p)
	if err == nil {
		return maxCount(ct), nil
	}
	return nil, err
}
